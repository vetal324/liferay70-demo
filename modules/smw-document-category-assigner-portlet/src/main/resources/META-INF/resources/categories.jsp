<%@ include file="/init.jsp" %>
<%@ taglib prefix="template" uri="http://liferay.com/tld/categories" %>

<div id="categories-tree-wrapper">
    <div class="message-container">
        <div class="portlet-msg-success" style="display: none"></div>
        <div class="portlet-msg-error" style="display: none"></div>
    </div>
    <h2><liferay-ui:message key="smw-category-select-category" /></h2>
    <h3><liferay-ui:message key="smw-category-global" /></h3>
    <div id="categories-tree" class="tree-wrapper">
        <ul>
            <c:forEach var="vocabulary" items="${categoryModel.vocabularies}">
                <li data-jstree='{"type":"vocabulary"}'>
                        ${vocabulary.name}
                    <c:forEach var="category" items="${vocabulary.categories}">
                        <template:categories category="${category}"/>
                    </c:forEach>
                </li>
            </c:forEach>
        </ul>
    </div>
    <div class="text-right">
        <a href="javascript:;" class="btn btn-primary" id="assignCategoriesBtn">
            <liferay-ui:message key="smw-category-btn-assign" />
        </a>
    </div>
</div>
