<%@ include file="/init.jsp" %>
<%@ taglib prefix="template" uri="http://liferay.com/tld/folders" %>

<div id="document-category-assigner-wrapper">
    <div id="documents-tree-wrapper">
        <div class="message-container">
            <div class="portlet-msg-success" style="display: none"></div>
            <div class="portlet-msg-error" style="display: none"></div>
        </div>
        <h2><liferay-ui:message key="smw-category-select-document" /></h2>
        <div id="documents-tree" class="tree-wrapper">
            <ul>
                <c:forEach var="folder" items="${dlModel.folders}">
                    <template:folders folder="${folder}"/>
                </c:forEach>
                <c:forEach var="file" items="${dlModel.files}">
                    <li data-jstree='{"type":"file"}' fileId="${file.fileEntryId}">${file.name}</li>
                </c:forEach>
            </ul>
        </div>
        <div class="text-right">
            <a href="javascript:;" class="btn btn-default" id="toggleAllBtn">
                <liferay-ui:message key="smw-category-btn-select-deselect-all" />
            </a>
        </div>
        <div>
            <p>
                <liferay-ui:message key="smw-category-hint-select-only-one" />
            </p>
        </div>
        <div class="text-right">
            <a href="javascript:;" class="btn btn-default" id="removeAllCategoriesBtn">
                <liferay-ui:message key="smw-category-btn-remove-all-categories" />
            </a>
            <a href="javascript:;" class="btn btn-primary" id="manageAllCategoriesBtn">
                <liferay-ui:message key="smw-category-btn-manage-categories" />
            </a>
        </div>
    </div>
    <div id="categories-popup">
    </div>
</div>