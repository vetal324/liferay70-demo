<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:resourceURL var="viewCategoriesUrl" id="/categories/view" />
<portlet:resourceURL var="assignCategoriesUrl" id="/categories/assign"  />

<script type="text/javascript">
    $(document).ready(function () {
        var namespace = '<portlet:namespace/>';
        var config = {
            namespace: namespace,
            containerId: 'document-category-assigner-wrapper',
            documentsTreeId: 'documents-tree',
            categoriesTreeId: 'categories-tree',
            categoriesPopupId: 'categories-popup',
            urls: {
                viewCategoriesUrl: '${viewCategoriesUrl}',
                assignCategoriesUrl: '${assignCategoriesUrl}'
            }
        };
        var categoryAssigner = new CategoryAssigner(config);
        categoryAssigner.initialize();
        window.categoryAssigner = categoryAssigner; //for debug
    });
</script>