var CategoryAssigner = function (config) {

    this.namespace = config.namespace;

    this.urls = config.urls;

    this.containerId = config.containerId;
    this.container = $("#" + this.containerId);

    this.categoriesPopupId = config.categoriesPopupId;
    this.documentsTree = this.container.find("#" + config.documentsTreeId);
    this.categoriesTreeId = config.categoriesTreeId;

    this.aui = new AUI();

    this.categoriesModal = undefined;
    this.categoriesTree = undefined;

    this.selectedFiles = [];
};

$.extend(true, CategoryAssigner, {

    prototype: {

        /*======================================= Initialization =====================================================*/
        initialize: function() {

            this.initDocumentsTree();

            this.bindListeners();
        },

        initDocumentsTree(){
            this.documentsTree.jstree({
                "checkbox": {
                    "keep_selected_style": false
                },
                types: {
                    "file": {
                        "icon": "icon-file"
                    },
                    "default": {}
                },
                "plugins": ["checkbox", "types"]
            });
        },

        initCategoriesTree(scope){
            scope.categoriesTree = scope.container.find("#" + this.categoriesTreeId);
            this.categoriesTree.jstree({
                "checkbox": {
                    "keep_selected_style": false
                },
                types: {
                    "vocabulary": {
                        "icon": false
                    },
                    "default": {}
                },
                "plugins": ["checkbox", "types"]
            }).bind("ready.jstree", function (event, data) {
                $('#' + scope.categoriesTreeId).jstree('open_all');
                $('li[checked=true]').each(function (i) {
                    $('#' + scope.categoriesTreeId).jstree('check_node', this);
                });
            });
        },

        /*======================================= Binding Listeners ==================================================*/
        bindListeners: function(){
            this.container.on("click", "#toggleAllBtn", {scope: this}, this.toggleAllHandler);
            this.container.on("click", "#removeAllCategoriesBtn", {scope: this}, this.removeAllCategoriesHandler);
            this.container.on("click", "#manageAllCategoriesBtn", {scope: this}, this.manageCategoriesHandler);
            this.container.on("click", "#assignCategoriesBtn", {scope: this}, this.assignCategoriesBtnHandler);
        },


        /*======================================= Handlers ===========================================================*/
        toggleAllHandler: function(event){
            var scope = event.data.scope;
            var documentsTree = scope.documentsTree;
            var toggleType = documentsTree.data("toggleType");
            if (toggleType === "uncheck") {
                documentsTree.jstree("uncheck_all");
                documentsTree.data("toggleType", "check");
            } else {
                documentsTree.jstree("check_all");
                documentsTree.data("toggleType", "uncheck");
            }
        },

        removeAllCategoriesHandler: function(event){
            var scope = event.data.scope;
            scope.selectedFiles = scope.getSelectedFileIds(scope);
            if (scope.selectedFiles !== '') {
                var assignCategoriesUrl = scope.urls.assignCategoriesUrl;
                var data = {};
                data[scope.namespace + "files"] = scope.selectedFiles;
                data[scope.namespace + "categories"] = "";
                scope.postData(assignCategoriesUrl, data)
                    .done(function (response) {
                        var responseJson = JSON.parse(response);
                        if (responseJson.success) {
                            scope.showSuccess(scope, responseJson.message, true);
                        } else {
                            scope.showError(scope, responseJson.message, false);
                        }
                    });
            } else {
                scope.showError(scope, 'Select at least one file.', false);
            }
        },

        manageCategoriesHandler: function(event){
            var scope = event.data.scope;
            scope.selectedFiles = scope.getSelectedFileIds(scope);
            if (scope.selectedFiles !== '') {
                var viewCategoriesUrl = scope.urls.viewCategoriesUrl;
                var data = {};
                data[scope.namespace + "files"] = scope.selectedFiles;
                scope.postData(viewCategoriesUrl, data)
                    .done(function (response) {
                        console.log("response: ", response);
                        scope.categoriesModal = new scope.aui.Modal({
                            headerContent: "Categories Assigner",
                            bodyContent: response,
                            destroyOnHide: true,
                            centered: true,
                            modal: true,
                            resizable: false,
                            render: "#" + scope.categoriesPopupId,
                            width: 800
                        });
                        scope.categoriesModal.render();
                        scope.initCategoriesTree(scope);
                    });
            } else {
                scope.showError(scope, 'Select at least one file.', false);
            }
        },

        assignCategoriesBtnHandler: function(event) {
            var scope = event.data.scope;
            var selectedCategories = scope.getSelectedCategories(scope);
            var assignCategoriesUrl = scope.urls.assignCategoriesUrl;
            var data = {};
            data[scope.namespace + "files"] = scope.selectedFiles;
            data[scope.namespace + "categories"] = selectedCategories;
            scope.postData(assignCategoriesUrl, data)
                .done(function (response) {
                    var responseJson = JSON.parse(response);
                    if (responseJson.success) {
                        scope.showSuccess(scope, responseJson.message, true);
                    } else {
                        scope.showError(scope, responseJson.message, false);
                    }
                })
                .fail(function (error) {
                    scope.showError(scope, error);
                });
        },

        /*======================================= Utils ==============================================================*/
        postData: function(url, data){
            return $.ajax({
                url: url,
                type: "POST",
                data: data
            });
        },

        getSelectedFileIds: function (scope) {
            var documentsTree = scope.documentsTree;
            var selectedElements = documentsTree.jstree("get_selected", true);
            return selectedElements
                .filter(el => el.li_attr.fileid)
                .map(el => el.li_attr.fileid)
                .join(",");
        },

        getSelectedCategories: function (scope) {
            var categoriesTree = scope.categoriesTree;
            var selectedElements = categoriesTree.jstree("get_selected", true);
            return selectedElements
                .filter(el => el.li_attr.categoryid)
                .map(el => el.li_attr.categoryid)
                .join(",");
        },

        showSuccess: function (scope, message, hide) {
            var successMsgBlock = scope.container.find(".message-container .portlet-msg-success");
            var errorMsgBlock = scope.container.find(".message-container .portlet-msg-error");
            successMsgBlock.html(message);
            successMsgBlock.show();
            errorMsgBlock.html("");
            errorMsgBlock.hide();
            setTimeout(function () {
                successMsgBlock.fadeOut();
                if (hide) {
                    scope.categoriesModal.hide();
                }
            }, 2000);
        },

        showError: function (scope, message, hide) {
            var successMsgBlock = scope.container.find(".message-container .portlet-msg-success");
            var errorMsgBlock = scope.container.find(".message-container .portlet-msg-error");
            successMsgBlock.html("");
            successMsgBlock.hide();
            errorMsgBlock.html(message);
            errorMsgBlock.show();
            setTimeout(function () {
                errorMsgBlock.fadeOut();
                if (hide) {
                    scope.categoriesModal.hide();
                }
            }, 2000);
        }

    }

});

window.CategoryAssigner = CategoryAssigner;