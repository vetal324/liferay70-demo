<%@tag description="display folders tree" pageEncoding="UTF-8" %>
<%@attribute name="folder" type="de.etecture.bmw.smw.category.assigner.model.Folder" required="true" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="template" uri="http://liferay.com/tld/folders" %>

<li> ${folder.folderName}
    <c:if test="${fn:length(folder.folders) > 0 || fn:length(folder.files) > 0}">
        <ul>
            <c:forEach var="childFolder" items="${folder.folders}">
                <template:folders folder="${childFolder}"/>
            </c:forEach>
            <c:forEach var="childFile" items="${folder.files}">
                <li data-jstree='{"type":"file"}' fileId="${childFile.fileEntryId}">${childFile.name}</li>
            </c:forEach>
        </ul>
    </c:if>
</li>