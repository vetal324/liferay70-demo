<%@tag description="display categories tree" pageEncoding="UTF-8" %>
<%@attribute name="category" type="de.etecture.bmw.smw.category.assigner.model.Category" required="true" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="template" uri="http://liferay.com/tld/categories" %>
<ul>
    <li checked=${category.checked} categoryId="${category.categoryId}"> ${category.name}
        <c:if test="${fn:length(category.categories) > 0}">
                <c:forEach var="category" items="${category.categories}">
                    <template:categories category="${category}"/>
                </c:forEach>
        </c:if>
    </li>
</ul>