package de.etecture.bmw.smw.category.assigner.model;

public class FileEntry extends GroupedModel {

    private long fileEntryId;
    private String name;

    public FileEntry(long fileEntryId, String name) {
        this.fileEntryId = fileEntryId;
        this.name = name;
    }

    public long getFileEntryId() {
        return fileEntryId;
    }

    public void setFileEntryId(long fileEntryId) {
        this.fileEntryId = fileEntryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
