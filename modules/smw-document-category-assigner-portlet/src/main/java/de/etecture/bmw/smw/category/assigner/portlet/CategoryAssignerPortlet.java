package de.etecture.bmw.smw.category.assigner.portlet;

import de.etecture.bmw.smw.category.assigner.constants.CategoryAssignerPortletKeys;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

@Component(
		immediate = true,
		property = {
				"com.liferay.portlet.display-category=category.hidden",
				"com.liferay.portlet.instanceable=true",
				"javax.portlet.init-param.template-path=/",
				"javax.portlet.init-param.view-template=/view.jsp",
				"javax.portlet.name=" + CategoryAssignerPortletKeys.PORTLET_ID,
				"javax.portlet.display-name=" + CategoryAssignerPortletKeys.DISPLAY_NAME,
				"javax.portlet.resource-bundle=content.Language",
				"javax.portlet.security-role-ref=power-user,user",
				"com.liferay.portlet.add-default-resource=true",
				"com.liferay.portlet.layout-cacheable=true",
				"com.liferay.portlet.private-request-attributes=false",
				"com.liferay.portlet.private-session-attributes=false",
				"com.liferay.portlet.render-weight=50",
				"com.liferay.portlet.use-default-template=true",
				"javax.portlet.expiration-cache=0",
				"com.liferay.portlet.css-class-wrapper=" + CategoryAssignerPortletKeys.CSS_CLASS_WRAPPER,
				"com.liferay.portlet.header-portlet-javascript=/js/jstree.js",
				"com.liferay.portlet.header-portlet-javascript=/js/category-assigner.js",
				"com.liferay.portlet.header-portlet-css=/css/main.css",
				"com.liferay.portlet.header-portlet-css=https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css"
		},
		service = Portlet.class
)
public class CategoryAssignerPortlet extends MVCPortlet {

}