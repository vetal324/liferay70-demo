package de.etecture.bmw.smw.category.assigner.service;

import com.liferay.document.library.kernel.model.DLFileEntry;
import de.etecture.bmw.smw.category.assigner.exception.CategoryAssignException;

import java.util.List;

public interface CategoryAssignerService {

    void assign(long[] categoryIds, List<DLFileEntry> fileEntries) throws CategoryAssignException;

}
