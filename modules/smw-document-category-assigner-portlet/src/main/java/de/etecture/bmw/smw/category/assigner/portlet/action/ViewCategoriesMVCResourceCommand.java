package de.etecture.bmw.smw.category.assigner.portlet.action;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.GroupConstants;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCResourceCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import de.etecture.bmw.smw.category.assigner.constants.CategoryAssignerPortletKeys;
import de.etecture.bmw.smw.category.assigner.model.CategoryModel;
import de.etecture.bmw.smw.category.assigner.service.CategoryService;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

@Component(
        immediate = true,
        property = {
                "javax.portlet.name="+ CategoryAssignerPortletKeys.PORTLET_ID,
                "mvc.command.name=/categories/view"
        },
        service = MVCResourceCommand.class
)
public class ViewCategoriesMVCResourceCommand extends BaseMVCResourceCommand {

    @Override
    protected void doServeResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws Exception {
        CategoryModel categoryModel = null;
        String selectedFiles = resourceRequest.getParameter("files");
        long fileEntryId = 0;
        if (!Validator.isBlank(selectedFiles) && !selectedFiles.contains(",")) {
            try {
                fileEntryId = Long.parseLong(selectedFiles);
            } catch (Exception e) {
                _log.error(e.getMessage(), e);
            }
        }
        try {
            long globalGroupId = getGlobalGroupId(resourceRequest);
            categoryModel = _categoryService.buildCategoryModel(globalGroupId, fileEntryId);
        } catch (Exception e) {
            _log.error(e.getMessage());
        }
        resourceRequest.setAttribute("selectedFiles", selectedFiles);
        resourceRequest.setAttribute("categoryModel", categoryModel);
        include(resourceRequest, resourceResponse, "/categories.jsp");
    }

    private long getGlobalGroupId(ResourceRequest resourceRequest) {
        long companyId = _portal.getCompanyId(resourceRequest);
        Group globalGroup = _groupLocalService.fetchFriendlyURLGroup(companyId, GroupConstants.GLOBAL_FRIENDLY_URL);
        return globalGroup.getGroupId();
    }

    @Reference
    private Portal _portal;
    @Reference
    private CategoryService _categoryService;
    @Reference
    private GroupLocalService _groupLocalService;

    private static final Log _log = LogFactoryUtil.getLog(ViewCategoriesMVCResourceCommand.class);
}
