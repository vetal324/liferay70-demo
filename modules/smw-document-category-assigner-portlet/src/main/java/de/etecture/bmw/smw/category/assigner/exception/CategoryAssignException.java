package de.etecture.bmw.smw.category.assigner.exception;

public class CategoryAssignException extends Exception {

    public CategoryAssignException() {
        super();
    }

    public CategoryAssignException(String message) {
        super(message);
    }

    public CategoryAssignException(String message, Throwable cause) {
        super(message, cause);
    }

    public CategoryAssignException(Throwable cause) {
        super(cause);
    }

}
