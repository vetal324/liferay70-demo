package de.etecture.bmw.smw.category.assigner.model;

public class GroupedModel extends InstancedModel {

    private long groupId;

    public long getGroupId() {
        return groupId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }
}
