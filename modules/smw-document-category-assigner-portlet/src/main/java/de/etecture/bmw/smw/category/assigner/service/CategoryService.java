package de.etecture.bmw.smw.category.assigner.service;

import de.etecture.bmw.smw.category.assigner.model.CategoryModel;

public interface CategoryService {

    CategoryModel buildCategoryModel(long groupId, long fileEntryId);

}
