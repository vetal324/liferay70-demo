package de.etecture.bmw.smw.category.assigner.model;

import java.util.List;

public class DLModel extends GroupedModel {

    private List<Folder> folders;
    private List<FileEntry> files;

    public List<Folder> getFolders() {
        return folders;
    }

    public void setFolders(List<Folder> folders) {
        this.folders = folders;
    }

    public List<FileEntry> getFiles() {
        return files;
    }

    public void setFiles(List<FileEntry> files) {
        this.files = files;
    }
}
