package de.etecture.bmw.smw.category.assigner.constants;

public class CategoryAssignerPortletKeys {

	public static final String DISPLAY_NAME = "SMW Category Portlet";

	public static final String PORTLET_ID = "de_etecture_bmw_smw_category_assigner_CategoryAssignerPortlet";

	public static final String CSS_CLASS_WRAPPER = "smw-category-portlet";

}