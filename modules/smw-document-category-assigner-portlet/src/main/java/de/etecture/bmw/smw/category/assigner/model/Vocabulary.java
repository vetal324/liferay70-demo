package de.etecture.bmw.smw.category.assigner.model;

import java.util.List;

public class Vocabulary extends GroupedModel {

    private long vocabularyId;
    private String name;

    private List<Category> categories;

    public long getVocabularyId() {
        return vocabularyId;
    }

    public void setVocabularyId(long vocabularyId) {
        this.vocabularyId = vocabularyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }
}
