package de.etecture.bmw.smw.category.assigner.service.impl;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.asset.kernel.service.AssetVocabularyLocalService;
import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.service.DLFileEntryLocalService;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import de.etecture.bmw.smw.category.assigner.model.Category;
import de.etecture.bmw.smw.category.assigner.model.CategoryModel;
import de.etecture.bmw.smw.category.assigner.model.Vocabulary;
import de.etecture.bmw.smw.category.assigner.service.CategoryService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import java.util.ArrayList;
import java.util.List;

@Component
public class CategoryServiceImpl implements CategoryService {

    private List<Long> fileCategoryIds = new ArrayList<>();

    @Override
    public CategoryModel buildCategoryModel(long groupId, long fileEntryId) {
        CategoryModel categoryModel = new CategoryModel();
        List<Vocabulary> vocabularies = new ArrayList<>();
        fileCategoryIds = new ArrayList<>();
        try {
            if (fileEntryId > 0) {
                DLFileEntry dlFileEntry = _dlFileEntryLocalService.fetchDLFileEntry(fileEntryId);
                List<AssetCategory> fileCategories = _assetCategoryLocalService.getCategories(DLFileEntry.class.getName(), dlFileEntry.getFileEntryId());
                fileCategories.forEach(cat -> fileCategoryIds.add(cat.getCategoryId()));
            }
            List<AssetVocabulary> assetVocabularies = _assetVocabularyLocalService.getGroupVocabularies(groupId);
            assetVocabularies.forEach(voc -> vocabularies.add(toVocabulary(voc)));
        } catch (PortalException e) {
            _log.error(e.getMessage());
        }
        categoryModel.setVocabularies(vocabularies);
        return categoryModel;
    }

    private Vocabulary toVocabulary(AssetVocabulary assetVocabulary) {
        Vocabulary vocabulary = new Vocabulary();
        List<AssetCategory> assetCategories = _assetCategoryLocalService.getVocabularyRootCategories(
                assetVocabulary.getVocabularyId(),
                QueryUtil.ALL_POS,
                QueryUtil.ALL_POS,
                null);
        List<Category> categories = new ArrayList<>();
        for (AssetCategory assetCategory : assetCategories) {
            categories.add(toCategory(assetCategory));
        }
        vocabulary.setVocabularyId(assetVocabulary.getVocabularyId());
        vocabulary.setCompanyId(assetVocabulary.getCompanyId());
        vocabulary.setGroupId(assetVocabulary.getGroupId());
        vocabulary.setName(assetVocabulary.getName());
        vocabulary.setCategories(categories);

        return vocabulary;
    }

    private Category toCategory(AssetCategory assetCategory) {
        Category category = new Category();
        category.setCategoryId(assetCategory.getCategoryId());
        category.setCompanyId(assetCategory.getCompanyId());
        category.setGroupId(assetCategory.getGroupId());
        category.setName(assetCategory.getName());
        boolean isSelected = fileCategoryIds.contains(assetCategory.getCategoryId());
        category.setChecked(isSelected);
        List<Category> categories = new ArrayList<>();
        List<AssetCategory> assetCategories = _assetCategoryLocalService.getChildCategories(assetCategory.getCategoryId());
        for(AssetCategory cat : assetCategories) {
            categories.add(toCategory(cat));
        }
        category.setCategories(categories);
        return category;
    }

    @Reference
    private DLFileEntryLocalService _dlFileEntryLocalService;
    @Reference
    private AssetVocabularyLocalService _assetVocabularyLocalService;
    @Reference
    private AssetCategoryLocalService _assetCategoryLocalService;

    private static final Log _log = LogFactoryUtil.getLog(CategoryServiceImpl.class);
}
