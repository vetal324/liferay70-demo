package de.etecture.bmw.smw.category.assigner.portlet.action;

import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.service.DLFileEntryLocalService;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import de.etecture.bmw.smw.category.assigner.constants.CategoryAssignerPortletKeys;
import de.etecture.bmw.smw.category.assigner.service.CategoryAssignerService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@Component(
        immediate = true,
        property = {
                "javax.portlet.name=" + CategoryAssignerPortletKeys.PORTLET_ID,
                "mvc.command.name=/categories/assign"
        },
        service = MVCResourceCommand.class
)
public class AssignCategoriesMVCResourceCommand implements MVCResourceCommand {

    @Override
    public boolean serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) {

        try {
            long[] fileIds = ParamUtil.getLongValues(resourceRequest, "files");
            long[] categoryIds = ParamUtil.getLongValues(resourceRequest, "categories");
            List<DLFileEntry> files = new ArrayList<>();
            for (long fileId : fileIds) {
                DLFileEntry file = _dlFileEntryLocalService.getDLFileEntry(fileId);
                files.add(file);
            }
            _categoryAssignerService.assign(categoryIds, files);
            sendResponseMessage(resourceResponse,true, "Categories have been updated successfully.");
            return false;
        } catch (Exception e) {
            sendResponseMessage(resourceResponse,false, e.getMessage());
            return true;
        }

    }

    private void sendResponseMessage(ResourceResponse resourceResponse, boolean success, String message) {
        try {
            JSONObject responseJson = JSONFactoryUtil.createJSONObject();
            responseJson.put("success", String.valueOf(success));
            responseJson.put("message", message);
            PrintWriter printWriter = resourceResponse.getWriter();
            printWriter.println(responseJson.toString());
        } catch (Exception e) {
            _log.error("Failed to send response message, cause: " + e.getMessage());
        }
    }


    @Reference
    private DLFileEntryLocalService _dlFileEntryLocalService;
    @Reference
    private CategoryAssignerService _categoryAssignerService;


    private static final Log _log = LogFactoryUtil.getLog(AssignCategoriesMVCResourceCommand.class);

}
