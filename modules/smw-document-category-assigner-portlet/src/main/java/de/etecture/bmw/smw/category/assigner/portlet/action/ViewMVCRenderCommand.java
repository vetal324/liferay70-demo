package de.etecture.bmw.smw.category.assigner.portlet.action;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.util.Portal;
import de.etecture.bmw.smw.category.assigner.constants.CategoryAssignerPortletKeys;
import de.etecture.bmw.smw.category.assigner.model.DLModel;
import de.etecture.bmw.smw.category.assigner.service.DocumentService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

@Component(
        immediate = true,
        property = {
                "javax.portlet.name=" + CategoryAssignerPortletKeys.PORTLET_ID,
                "mvc.command.name=/",
                "mvc.command.name=/documents/view"
        },
        service = MVCRenderCommand.class
)
public class ViewMVCRenderCommand implements MVCRenderCommand {

    @Override
    public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
        DLModel dlModel = null;
        try {
            long groupId = _portal.getScopeGroupId(renderRequest);
            dlModel = _documentService.buildDLModel(groupId);
        } catch (Exception e) {
            _log.error(e.getMessage());
        }
        renderRequest.setAttribute("dlModel", dlModel);
        return "/view.jsp";
    }


    @Reference
    private Portal _portal;
    @Reference
    private DocumentService _documentService;

    private static final Log _log = LogFactoryUtil.getLog(ViewMVCRenderCommand.class);

}
