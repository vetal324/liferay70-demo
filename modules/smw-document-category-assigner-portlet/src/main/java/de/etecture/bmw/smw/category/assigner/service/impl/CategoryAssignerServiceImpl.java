package de.etecture.bmw.smw.category.assigner.service.impl;

import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.service.DLAppLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.repository.model.FileEntry;
import de.etecture.bmw.smw.category.assigner.exception.CategoryAssignException;
import de.etecture.bmw.smw.category.assigner.service.CategoryAssignerService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import java.util.List;

@Component
public class CategoryAssignerServiceImpl implements CategoryAssignerService {

    @Override
    public void assign(long[] categoryIds, List<DLFileEntry> fileEntries) throws CategoryAssignException {
        try {
            for (DLFileEntry dlFileEntry : fileEntries) {
                FileEntry fileEntry = _dlAppLocalService.getFileEntry(dlFileEntry.getFileEntryId());
                long userId = fileEntry.getUserId();
                _dlAppLocalService.updateAsset(userId, fileEntry, fileEntry.getFileVersion(), categoryIds, null, null);
            }
        } catch (PortalException e) {
            throw new CategoryAssignException("Failed to assign categories, cause: " + e.getMessage(), e);
        }
    }

    @Reference
    private DLAppLocalService _dlAppLocalService;

}
