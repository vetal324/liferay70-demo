package de.etecture.bmw.smw.category.assigner.service;

import de.etecture.bmw.smw.category.assigner.model.DLModel;

public interface DocumentService {

    DLModel buildDLModel(long groupId);

}
