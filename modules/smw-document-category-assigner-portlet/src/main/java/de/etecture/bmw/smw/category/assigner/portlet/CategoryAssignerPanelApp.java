package de.etecture.bmw.smw.category.assigner.portlet;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.application.list.constants.PanelCategoryKeys;
import com.liferay.portal.kernel.model.Portlet;
import de.etecture.bmw.smw.category.assigner.constants.CategoryAssignerPortletKeys;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
        immediate = true,
        property = {
                "panel.app.order:Integer=201",
                "panel.category.key=" + PanelCategoryKeys.SITE_ADMINISTRATION_CONTENT
        },
        service = PanelApp.class
)
public class CategoryAssignerPanelApp extends BasePanelApp {

    @Override
    public String getPortletId() {
        return CategoryAssignerPortletKeys.PORTLET_ID;
    }

    @Override
    @Reference(
            target = "(javax.portlet.name=" + CategoryAssignerPortletKeys.PORTLET_ID + ")",
            unbind = "-"
    )
    public void setPortlet(Portlet portlet) {
        super.setPortlet(portlet);
    }

}
