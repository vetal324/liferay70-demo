package de.etecture.bmw.smw.category.assigner.model;

import java.util.List;

public class CategoryModel {

    private List<Vocabulary> vocabularies;

    public List<Vocabulary> getVocabularies() {
        return vocabularies;
    }

    public void setVocabularies(List<Vocabulary> vocabularies) {
        this.vocabularies = vocabularies;
    }
}
