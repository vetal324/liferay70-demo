package de.etecture.bmw.smw.category.assigner.service.impl;

import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.model.DLFolder;
import com.liferay.document.library.kernel.model.DLFolderConstants;
import com.liferay.document.library.kernel.service.DLFileEntryLocalService;
import com.liferay.document.library.kernel.service.DLFolderLocalService;
import com.liferay.portal.kernel.util.ListUtil;
import de.etecture.bmw.smw.category.assigner.model.DLModel;
import de.etecture.bmw.smw.category.assigner.model.FileEntry;
import de.etecture.bmw.smw.category.assigner.model.Folder;
import de.etecture.bmw.smw.category.assigner.service.DocumentService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import java.util.ArrayList;
import java.util.List;

@Component
public class DocumentServiceImpl implements DocumentService {

    @Override
    public DLModel buildDLModel(long groupId) {
        DLModel dlModel = new DLModel();
        long parentFolderId = DLFolderConstants.DEFAULT_PARENT_FOLDER_ID;

        List<DLFileEntry> dlFileEntries = dlFileEntryLocalService.getFileEntries(groupId, parentFolderId);
        List<FileEntry> fileEntries = toFiles(dlFileEntries);
        dlModel.setFiles(fileEntries);

        List<DLFolder> dlFolders = dlFolderLocalService.getFolders(groupId, parentFolderId);
        List<Folder> folders = toFolders(dlFolders);
        dlModel.setFolders(folders);

        return dlModel;
    }

    private List<Folder> toFolders(List<DLFolder> dlFolders) {
        List<Folder> folders = new ArrayList<>();
        if (ListUtil.isNotEmpty(dlFolders)) {
            for (DLFolder dlFolder: dlFolders) {
                if (dlFolder.isHidden()) {
                    continue;
                }
                Folder folder = toFolder(dlFolder);
                folders.add(folder);
            }
        }
        return folders;
    }

    private Folder toFolder(DLFolder dlFolder) {
        Folder folder = new Folder();
        folder.setFolderId(dlFolder.getFolderId());
        folder.setFolderName(dlFolder.getName());

        long groupId = dlFolder.getGroupId();
        long folderId = dlFolder.getFolderId();

        List<DLFolder> dlFolders = dlFolderLocalService.getFolders(groupId, folderId);
        if (ListUtil.isNotEmpty(dlFolders)) {
            List<Folder> folders = toFolders(dlFolders);
            folder.setFolders(folders);
        }

        List<DLFileEntry> dlFileEntries = dlFileEntryLocalService.getFileEntries(groupId, folderId);
        if (ListUtil.isNotEmpty(dlFileEntries)) {
            List<FileEntry> files = toFiles(dlFileEntries);
            folder.setFiles(files);
        }

        return folder;
    }

    private List<FileEntry> toFiles(List<DLFileEntry> dlFileEntries) {
        List<FileEntry> fileEntries = new ArrayList<>();
        if (ListUtil.isNotEmpty(dlFileEntries)) {
            for (DLFileEntry dlFileEntry : dlFileEntries) {
                FileEntry fileEntry = toFile(dlFileEntry);
                fileEntries.add(fileEntry);
            }
        }
        return fileEntries;
    }

    private FileEntry toFile(DLFileEntry dlFileEntry) {
        FileEntry fileEntry = new FileEntry(dlFileEntry.getFileEntryId(), dlFileEntry.getTitle());
        fileEntry.setGroupId(dlFileEntry.getGroupId());
        fileEntry.setCompanyId(dlFileEntry.getCompanyId());
        return fileEntry;
    }


    @Reference
    private DLFileEntryLocalService dlFileEntryLocalService;
    @Reference
    private DLFolderLocalService dlFolderLocalService;
}
