package de.etecture.bmw.smw.category.assigner.model;

public class InstancedModel {

    private long companyId;

    public long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(long companyId) {
        this.companyId = companyId;
    }
}
